# Import libraries
import requests
import urllib.request
import time
from bs4 import BeautifulSoup
import csv

# Set the URL you want to webscrape from
url = 'https://fuelprice.io/vic/melbourne/?show_stations=1#nearby-stations'

response = requests.get(url)

soup = BeautifulSoup(response.text, "html.parser")

all_fuel_container = soup.find_all('ul', class_ = 'all-stations')

fuel_list = all_fuel_container[0].find_all("li")





with open('data.csv', 'w') as csvFile:
    for fuel_station in fuel_list:
        # print(fuel_station.text)
        fuelString = fuel_station.text
        stationName = fuelString.split('~')[0]
        stationPrice = fuelString.split('~')[1].split()[0]
        print(stationName + " " + stationPrice)
    writer = csv.writer(csvFile)
    writer.writeRows(stationName + " " + stationPrice)

csvFile.close()